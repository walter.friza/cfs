# Set output file
set(ofile "@CMAKE_CURRENT_BINARY_DIR@/ztest.out")
set(SUPERLU_ZTEST_EXEC "@SUPERLU_ZTEST_EXEC@")
set(SUPERLU_SOURCE_DIR "@SUPERLU_SOURCE_DIR@")

if(EXISTS "${ofile}")
  file(REMOVE "${ofile}")
endif()

file(APPEND "${ofile}" "Double-precision complex testing output\n")

set(MATRICES LAPACK cg20.cua)
set(NVAL 9 19)
set(NRHS 5)
set(LWORK 0 10000000)

# Loop through all matrices ...
foreach(m ${MATRICES})
  #--------------------------------------------
  # Test matrix types generated in LAPACK-style
  #--------------------------------------------
  if (${m} STREQUAL "LAPACK")
    file(APPEND "${ofile}" "== LAPACK test matrices\n")
    foreach(n ${NVAL})
      foreach(s ${NRHS})
	foreach(l ${LWORK})
	  file(APPEND "${ofile}" "\n")
	  file(APPEND "${ofile}" "n=${n} nrhs=${s} lwork=${l}\n")
	  execute_process(COMMAND "${SUPERLU_ZTEST_EXEC}" -t "LA" -l ${l} -n ${n} -s ${s} OUTPUT_VARIABLE test_out ERROR_VARIABLE test_out)
	  file(APPEND "${ofile}" "${test_out}")
	endforeach(l ${LWORK})
      endforeach(s ${NRHS})
    endforeach(n ${NVAL})
    #--------------------------------------------
    # Test a specified sparse matrix
    #--------------------------------------------
  else (${m} STREQUAL "LAPACK")
    file(APPEND "${ofile}" "\n")
    file(APPEND "${ofile}" "== sparse matrix: ${m}\n")
    foreach(s ${NRHS})
      foreach(l ${LWORK})
	file(APPEND "${ofile}" "\n")
	file(APPEND "${ofile}" "nrhs=${s} lwork=${l}\n")
	execute_process(COMMAND "${SUPERLU_ZTEST_EXEC}" -t "SP" -s ${s} -l ${l} INPUT_FILE "${SUPERLU_SOURCE_DIR}/examples/${m}" OUTPUT_VARIABLE test_out ERROR_VARIABLE test_out)
	file(APPEND "${ofile}" "${test_out}")
      endforeach(l ${LWORK})
    endforeach(s ${NRHS})
  endif (${m} STREQUAL "LAPACK")
endforeach(m ${MATRICES})

# Local Variables:
# tab-width: 8
# mode: cmake
# indent-tabs-mode: t
# End:
# ex: shiftwidth=2 tabstop=8
